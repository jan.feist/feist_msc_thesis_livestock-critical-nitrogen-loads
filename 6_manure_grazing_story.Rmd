---
title: "X_manure_grazing_story"
author: "Jan Feist"
date: "2023-04-24"
output: html_document
editor_options: 
  chunk_output_type: console
---

This code plots the total amount of manure per NUTS2 region and the total amount of manure per ha per NUTS2 region.

```{r setup, include=FALSE}
library(readr)
library(tidyr)
library(dplyr)
library(ggplot2)
library(viridis)
```

```{r}
Manuredir <- "//fibl.ch/FILES/Dep_Socioeco/2_Projects/2021_EU-PATHWAYS_3521501/workpackages/WP6/geo-sol explore/Manure_computation/output/"
NH3dir <- "//fibl.ch/files/Dep_Socioeco/2_Projects/2021_EU-PATHWAYS_3521501/workpackages/WP6/geo-sol explore/NH3_emission/output/"

```

```{r}
Eurostat_Devries <- read_delim(paste0(Manuredir, "Eurostat_Devries_20230217_20230313.csv"))
NH3_testing_distinct <- read_delim(paste0(NH3dir, "NH3.csv"))
```

```{r}
manure <- Eurostat_Devries %>% 
  filter(!is.na(`kgN/year`)) 

ggplot(manure, aes(fill=`de vries`, x=`Country code`, y=`kgN/year`)) + 
  geom_bar(position="stack", stat="identity") +
  ggtitle("Nex per country per species") +
  ylab("Nex total") +
  xlab("country") +
    scale_fill_viridis(discrete = T)

ggplot(manure, aes(fill=`de vries`, x=`Country code`, y=`kgN/year`)) + 
  geom_bar(position="fill", stat="identity") +
  ggtitle("Nex per country per species") +
  ylab("Nex total") +
  xlab("country") +
    scale_fill_viridis(discrete = T)
```

# Display the dominant species in terms of NEx per country
```{r}
manure2 <- Eurostat_Devries %>%
  filter(!is.na(`kgN/year`)) %>%
  group_by(`Country code`, `de vries`) %>%
  summarise(total_Nex = sum(`kgN/year`)) %>%
  ungroup() %>%
  group_by(`Country code`) %>%
  arrange(total_Nex, by_group = T)

```

```{r}

manure3 <- NH3_testing_distinct %>%
  group_by(CNTR_CODE, method) %>%
  summarise(total_NH3 = sum(e_mms_nh3),
            N_grazeland = sum(m_graz_n)) %>%
  filter(!is.na(N_grazeland))

# total NH3 emission per country
ggplot(manure3, aes(fill=method, x=CNTR_CODE, y=total_NH3)) + 
  geom_bar(position="dodge", stat="identity") +
  ggtitle("Total NH3 emission per country") +
  ylab("kgN/year") +
  xlab("Country")

# N deposited in grazeland
ggplot(manure3, aes(fill=method, x=CNTR_CODE, y=N_grazeland)) + 
  geom_bar(position="dodge", stat="identity") +
  ggtitle("Total N deposited in grazeland") +
  ylab("kgN/year") +
  xlab("Country")
  
```

# Plot N deposited in grazeland per ha
```{r}
sf_NH3_per_ha <- st_read(paste0(NH3dir, "NH3_per_ha.gpkg"))

df_NH3_per_ha <- sf_NH3_per_ha %>% 
  st_drop_geometry() %>%
  group_by(CNTR_CODE, method) %>%
  summarise(mean_N_graz_ha = mean(N_graz_ha)) %>%
  filter(!is.na(mean_N_graz_ha))

ggplot(manure3, aes(fill=method, x=CNTR_CODE, y=N_grazeland)) + 
  geom_bar(position="dodge", stat="identity") +
  ggtitle("Mean N deposited in grazeland") +
  ylab("kgN/year/ha") +
  xlab("Country")
```

